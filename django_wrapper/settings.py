"""
Django settings for django_wrapper project.

Generated by 'django-admin startproject' using Django 3.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""
from pathlib import Path
import os
# import django_heroku
import dj_database_url
import time

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
if "DJANGO_SECRET_KEY" not in os.environ.keys():
    SECRET_KEY = 'django-insecure-ut6*mb3i8jruw5mj4*t%8x2e^v7_ejx*$&ss6xpm^sv@syxf4y'
else:
    SECRET_KEY = os.environ['DJANGO_SECRET_KEY']


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG']=='True' :
    DEBUG = True
    print("DEBUGGING!!")

ALLOWED_HOSTS = [
    'https://ec2-63-34-84-98.eu-west-1.compute.amazonaws.com',
    'http://ec2-63-34-84-98.eu-west-1.compute.amazonaws.com',
    'ec2-63-34-84-98.eu-west-1.compute.amazonaws.com',
    'localhost', '127.0.0.1'
    ]
# 'https://biz-report-gui.herokuapp.com',


# Application definition

INSTALLED_APPS = [
    'rest_framework',
    'brands.apps.BrandsConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'django_wrapper.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_wrapper.wsgi.application'

# DATABASE_URL = "postgres://YourUserName:YourPassword@localHost:5432/YourDatabaseName";
# postgres://postgres:<passowrdsecret>@localhost:5432/dbname

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

SSL_KEYS = {
    'ca':    os.environ['PROD_RW_SSL_CA_FILE'],
    'cert':  os.environ['PROD_RW_SSL_CERT_FILE'],
    'key':   os.environ['PROD_RW_SSL_KEY_FILE']
}


DATABASES = {
    # brand_details ....
    'brands': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "optival_prod",
        'USER': os.environ["PROD_DB_USERNAME"],
        'PASSWORD': os.environ["PROD_DB_PASSWORD"],
        'HOST': os.environ["PROD_RW_IPV4"],
        'PORT': '3306',
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
            'ssl': SSL_KEYS,
        },
        # 'OPTIONS': {
        #     'init_command': "SET sql_mode='STRICT_TRANS_TABLES'"
        # }
    },
}


# this is sqlite3 . one of the two defaults should be eclipsed
DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3','NAME': BASE_DIR / 'db.sqlite3',}

if "DATABASE_URL" in os.environ.keys() and "postgres" in os.environ["DATABASE_URL"]:
# this is postgresql based local
    DATABASES['default'] = dj_database_url.config(conn_max_age=600, ssl_require=True)

DATABASE_ROUTERS = ['brands.route.DbRouter']


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Configure Django App for Heroku.
# print(f"{DATABASES}")
# django_heroku.settings(locals())
# print(f"{DATABASES}")
# time.sleep(5)

if DEBUG : print(f"default db is {DATABASES['default']}")

if "THIS_IS_HEROKU_APP" not in os.environ.keys():
    if DEBUG: print("removing OPTIONS sslmode")
    # if 'OPTIONS' in DATABASES['default'].keys():
    #     del DATABASES['default']['OPTIONS']['sslmode']
else:
    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": os.environ.get('REDIS_URL'),
            "OPTIONS": {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
            }
        }
    }
    if DEBUG: print(f"creating CACHES {CACHES}")

if DEBUG:
    os.environ.setdefault('DBG_JS_DBS',str(DATABASES))
    print(str(DATABASES))
