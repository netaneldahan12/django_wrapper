from django import forms

class NameForm(forms.Form):
    searched = forms.CharField(label='Requested string to look for', max_length=100)