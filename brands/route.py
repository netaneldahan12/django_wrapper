from django.conf import settings

class DbRouter:
    """
    A router to control all database operations on models in the
    user application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read user models go to users_db.
        """
        # if hasattr(model, 'process_id') and model._meta.app_label == 'remote':
        #     return Process.objects.get(id=model.process_id).environment
        # elif model._meta.app_label in ['remote', 'business_development']:
        #     return settings.REMOTE_DB_CONNECTION
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write user models go to users_db.
        """
        # if hasattr(model, 'process_id') and model._meta.app_label == 'remote':
        #     return Process.objects.get(id=model.process_id).environment
        # elif model._meta.app_label in ['remote', 'business_development']:
        #     return settings.REMOTE_DB_CONNECTION
        #
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the user app is involved.
        # """
        # if hasattr(obj1, 'process_id') and hasattr(obj2, 'process_id') and \
        #         Process.objects.get(id=obj1.process_id).environment == Process.objects.get(id=obj2.process_id).environment:
        #     return True
        # elif obj1._meta.app_label == 'remote' or \
        #    obj2._meta.app_label == 'remote':
        #    return True

        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth app only appears in the 'users_db'
        database.
        """
        # if app_label == 'brands':
        #     return db == 'brands'
        return None
