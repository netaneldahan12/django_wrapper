import datetime
import time
import sys
import os
import glob

#os.environ['RUN_MTD_FOLDER'] = '.'
try:
    os.environ['USERPROFILE']
    os.environ.setdefault("HOME",os.environ['USERPROFILE'])
except:
    pass

def get_ignored_terms():
    ignored = []
    if 'USE_IGNORED_TERMS_FILE' in os.environ.keys():
        with open(f"{os.environ['HOME']}{os.sep}.ignore_terms_on_logs","r") as frw:  alllinesstr=frw.read()
        with open("/tmp/ignoredlines","w") as ignfile: print(alllinesstr,file=ignfile)
        ignored = alllinesstr.split("\n")
    if ignored[-1] == '' : del ignored[-1]
    return ignored


def show_log(fn='',ignored=[]):
    filtered_log_lines = []
    # print("\n",fn,"\n\n")
    with open(fn, "r") as frw:
        alllinesstr = frw.read()
    fullcontent = alllinesstr.split("\n")
    working=0
    done_lines = {}
    total_subbrands = 1
    for doneline in fullcontent:
        if doneline.startswith("-------- *** DONE .") :
            # print("done line",oneline, file=sys.stderr)
            try: (myhead,myuniq,mytail) = doneline.split("|")
            except: myuniq="UNKNOWN b/c NO PIPE|"
            done_lines[myuniq] = doneline
            continue
    startofrun = None
    for oneline in fullcontent:
        if "Working" in oneline: working += 1
        # if oneline.startswith("-------- ***  Working on brand"):
        #     (myhead,myuniq,mytail) = oneline.split("|")
        #     if myuniq in done_lines.keys():
        #         stripped = done_lines[myuniq].strip().rstrip("\n")
        #         print("uniqstripped", stripped, file=sys.stderr)
        #         if stripped != '': filtered_log_lines.append(stripped)
        #         continue
        #     else:
        #         stripped = oneline.strip().rstrip("\n")
        #         print("stripped", stripped, file=sys.stderr)
        #     if stripped != '': filtered_log_lines.append(stripped)
        if "BrandGroups" in oneline : brand_groups_line = oneline
        if "brands to be run" in oneline :
            total_subbrands_to_be_run_line = oneline
            total_subbrands,startofrun = total_subbrands_to_be_run_line.split("brands to be run",2)
        # print("ONELINE ===>",oneline)
        found = False
        for oneterm in ignored:
            # print("LOOK FOR",oneterm,".....")
            if oneterm in oneline:
                found = True
                break
        if not found:
            stripped = oneline.strip().rstrip("\n")
            if stripped != '':
                filtered_log_lines.append(stripped)
    filtered_log_lines.append("--")
    worked_percent = int(int(working)/int(total_subbrands)*100)
    filtered_log_lines.append(f"Working/ed on {working} ({worked_percent}%). ( left todo {int(total_subbrands)-int(working)}) - "
                              f" now time: {datetime.datetime.now().strftime('%H:%M')} (started {startofrun})")
    # if "ELAPSED" in filtered_log_lines: filtered_log_lines.append("Extra ____________line")

    return filtered_log_lines

def get_list_of_files( filepattern='', folder=''):
    orig_dir = os.getcwd()
    try:
        os.chdir(str(folder))
    except:
        print(f"ERROR could not reach {folder}")
        return []
    nowdir = os.getcwd()
    myfiles = glob.glob(filepattern)
    newlist = []
    for onefile in myfiles:
        newlist.append(f'{nowdir}{os.sep}{onefile}')
    os.chdir(orig_dir)
    # print("newlist",newlist)
    return newlist


def is_it_newest( p='*', folder='', max_age=100):
    cnt = 0
    while cnt < max_age:
        time.sleep(1)
        cnt += 1
        list_of_paths = get_list_of_files(filepattern=p, folder=folder)
        # print("list_of_paths",list_of_paths)
        if len(list_of_paths) > 0:
            latest_path = max(list_of_paths, key=os.path.getctime)
            return latest_path
            if latest_path.endswith("crdownload"): continue
            secs = ((time.time() - (os.stat(latest_path).st_mtime)))
            if (secs < max_age): return latest_path
    return None
