import sys
import os
from .models import Brand_Details
from .models import Brand_Grouping
import traceback
import datetime
import subprocess
import time

def get_br_grp_list():
    tmp = {}
    for e in Brand_Grouping.objects.all().using('brands'):
        tmp[e.brand_grouping_name]=1
    ltmp = []
    for mykey in tmp.keys(): ltmp.append(mykey)
    return ltmp

def get_dosubs_list(br_grp_name):
    chosen_grp_id = None
    dosubs = []
    for e in Brand_Grouping.objects.all().using('brands'):
        if e.brand_grouping_name == br_grp_name:
            chosen_grp_id = e.id
            break
    for e in Brand_Details.objects.all().using('brands'):
        if e.brand_group_details_id == chosen_grp_id:
            dosubs.append(f"{e.brand_display_name} |{e.brand_id}")
    return dosubs


def insert_letters_as_separator(brands_list):
    brands_list.sort()
    myletter = '9'
    last_item = brands_list[-1]
    current_item = ''
    last_index = 0
    while current_item != last_item:
        for i in range(last_index, len(brands_list)):
            if brands_list[i][0].upper() > myletter:
                myletter = brands_list[i][0].upper()
                current_item = brands_list[i]
                brands_list.insert(i, myletter)
                last_index = i
                break
            if brands_list[i] == last_item:
                current_item = last_item
                break
    return brands_list

def get_job_details(myline):
    print("myline ------> ",myline)
    hap=dynoid=''
    aa = myline.split(" ")
    for i in range(len(aa)):
        oneword = aa[i]
        if oneword == "--app" : hap = aa[i+1]
        if oneword == "--dyno" : dynoid = aa[i+1]
    job_details = f"{dynoid}__{hap}"
    return job_details

def run_remote_command(autorun_file):
    cmdfullpath = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
        print(f"In remote command {cmdfullpath} ______________ on {os.getcwd()} {datetime.datetime.now()}")
    cs=None
    # orig_dir = os.getcwd()
    # os.chdir(os.environ['OPTIVAL_TOOLS_MASTER_PYTHON'])
    try:
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Starting {cmdfullpath} on {os.getcwd()} {datetime.datetime.now()}")
        cs = subprocess.run(cmdfullpath, shell=True,  check=True ,  timeout=1200 , capture_output=True,universal_newlines=True )  # , stdout=subprocess.PIPE ,    print(end='')
        # os.chdir(orig_dir)
        fhw = open(f"/tmp/django_wrapper_cs_info_{os.getpid()}_bizrep","w")
        print(cs,file=fhw)
        fhw.close()
        fhw = open(cmdfullpath,"a")
        print("\t\t----out-----\n",cs.stdout,file=fhw)
        print("\t\t----err-----\n",cs.stderr,file=fhw)
        fhw.close()

        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print("cs is=",cs,file=sys.stderr)
        new_cmd_file_path = cmdfullpath.replace('.sh','.DONE')
        os.rename(cmdfullpath,new_cmd_file_path)
        return cs
    except :
        with open(cmdfullpath,"a") as file: print(traceback.format_exc(), file=file)
        print(f"ERROR {cmdfullpath} failed to execute ........ /tmp/django_wrapper_traceback .................")
        fhw = open("/tmp/django_wrapper_traceback","w")
        # print(f"Failure cs returncode ={cs.returncode} in {os.getcwd()}",file=fhw)
        print(cs,file=fhw)
        print(traceback.format_exc(),file=fhw)
        print(traceback.print_stack(),file=fhw)
        fhw.close()
        # os.chdir(orig_dir)
        new_cmd_file_path = cmdfullpath.replace('.sh','.FAILED')
        os.rename(cmdfullpath,new_cmd_file_path)
        return None

def get_heroku_exe():
    import platform
    if platform.system()=="Windows":
        return "heroku.cmd"
    if platform.system()=='Linux':
        return 'heroku'

def get_python_exe():
    import platform
    if platform.system()=="Windows":
        return "python.exe"
    if platform.system()=='Linux':
        return 'python3'

def touch_file(fname):
    with open(fname, 'a'):
        try:  # Whatever if file was already existing
            os.utime(fname, None)  # => Set current time anyway
        except OSError:
            pass  # File deleted between open() and os.utime() calls
