from django.urls import path

from . import views

app_name = 'brands'
urlpatterns = [
    path('', views.index, name='index'),
    path('running_processes/', views.running_processes, name='running_processes'),
    path('cron_jobs/', views.cron_jobs, name='cron_jobs'),
    path('last_legend_log/', views.last_legend_log, name='last_legend_log'),
    path('last_8_log/', views.last_8_log, name='last_8_log'),
    path('last_mtd_log/', views.last_mtd_log, name='last_mtd_log'),
    path('last_prev_log/', views.last_prev_log, name='last_prev_log'),
    path('last_8_full_log/', views.last_8_full_log, name='last_8_full_log'),
    path('last_mtd_full_log/', views.last_mtd_full_log, name='last_mtd_full_log'),
    path('last_prev_full_log/', views.last_prev_full_log, name='last_prev_full_log'),
    path('last_saas_log/', views.last_saas_log, name='last_saas_log'),
    path('last_data_collector_log/', views.last_data_collector_log, name='last_data_collector_log'),
    path('brand_searcher/', views.brand_searcher, name='brand_searcher'),
    path('<str:brand_grouping_name>/', views.subbrands, name='subbrands'),
    path('runwatch/<str:job_details>', views.runwatch, name='runwatch'),
]

# https://levelup.gitconnected.com/from-html-to-django-6d8344da11d6