import time
import traceback

from django.test import TestCase
import subprocess
import datetime
import os

def get_heroku_exe():
    import platform
    if platform.system()=="Windows":
        return "heroku.cmd"
    if platform.system()=='Linux':
        return 'heroku'

def get_heroku_ps_list():
    repeats = 0
    cmd=''
    cs=''
    while ( repeats < 20 ):
        try:
            cmd = [get_heroku_exe(),'ps','-a','biz-report']
            # print(f"Starting {cmd} on {os.getcwd()} {datetime.datetime.now()}")
            cs = subprocess.run(cmd, shell=False,  check=True ,  timeout=1200, cwd=os.getcwd() ,capture_output=True)  # , stdout=subprocess.PIPE , =True,    print(end='')
            if cs.stdout is None or 'No dynos on biz-report' in cs.stdout.decode('utf-8'):
                time.sleep(3)
                repeats += 1
            else:
                # print(cs.stdout)
                return cs.stdout
            # print()
            # print(cs.stderr)
            pass
        except :
            print(f"{cmd} failed to execute")
            print(f"Failure cs returncode ={cs.returncode} in {os.getcwd()}")
            print(traceback.format_exc)
            print(traceback.print_stack)
        time.sleep(2)

def show_run_results(ps):
    cmd = [get_heroku_exe(), 'logs', '-a', 'biz-report', '--dyno', ps]
    print(cmd,".......................")
    # print(f"Starting {cmd} on {os.getcwd()} {datetime.datetime.now()}")
    cs = subprocess.run(cmd, shell=False, check=True, timeout=1200, cwd=os.getcwd(), capture_output=True)
    return cs.stdout.decode('utf-8')

# print(get_heroku_ps_list())
( arr ) = get_heroku_ps_list().decode('utf-8').split("\n")
(ps_details) = arr[1].split(" ")
print(ps_details[0])
results=show_run_results(ps_details[0])
while 'State changed from up to complete' not in results:
    print(results)
    time.sleep(3)
    results = show_run_results(ps_details[0])

print(results)