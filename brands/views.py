import datetime
from datetime import timezone
import stat
import sys
import time
import os
import subprocess
import traceback

from django.shortcuts import get_object_or_404,render,HttpResponseRedirect
from django.urls import reverse
from django.views import generic

from .models import Brand_Details
from .models import Brand_Grouping
from .utils import *
from .mtd_log_utils import *
import psutil
from django.shortcuts import render
from .forms import NameForm


def index(request):
    template_name = 'brands/index.html'
    context_object_name = 'brand_details_list'
    brands_list = get_br_grp_list()
    brands_list = insert_letters_as_separator(brands_list)
    brands_list.append("$")
    brands_list.append("running_processes")
    brands_list.append("cron_jobs")
    brands_list.append("last_legend_log")
    brands_list.append("last_8_log")
    brands_list.append("last_mtd_log")
    brands_list.append("last_prev_log")
    brands_list.append("last_saas_log")
    brands_list.append("last_data_collector_log")
    brands_list.append("last_8_full_log")
    brands_list.append("last_mtd_full_log")
    brands_list.append("last_prev_full_log")
    brands_list.insert(0," ")
    brands_list.insert(0,"brand_searcher")
    # print(brands_list[0])
    return render(request, 'brands/index.html', {'brand_details_list': brands_list})

def run_by_post(request):
    if request.method=="POST":
        cmd = build_post_cmd(request, request.POST)
        if cmd is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return render(request, 'brands/subbrands.html', {'brand_grouping_name':brand_grouping_name, 'form': request.POST})
        print(f"Going Into remote command {cmd} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        job_details = cmd
        return HttpResponseRedirect(reverse('brands:runwatch_api', args=(job_details,)))
    return None

def subbrands(request, brand_grouping_name):
    d_enddate = datetime.datetime.now() # "2018-06-12T00:00"
    d_startdate = d_enddate - datetime.timedelta(days=7)
    enddate = f"{d_enddate.date()}T00:00"
    startdate = f"{d_startdate.date()}T00:00"
    dynoid=''
    heroku_app = ''
    if request.method=="POST":
        cmd = build_cmd(request, request.POST, brand_grouping_name)
        if cmd is None:
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return render(request, 'brands/subbrands.html', {'brand_grouping_name':brand_grouping_name, 'form': request.POST})
        # print(f"Going Into remote command {cmd} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        # cs = run_remote_command(cmd)
        # print("WE ARE AFTER REMOTE_COMMAND cs=",cs)
        # print(cs.output)
        # print("going to job details......* * * *  **  *  ** ** ")
        job_details = cmd
        return HttpResponseRedirect(reverse('brands:runwatch', args=(job_details,)))
    subbrands_list = get_dosubs_list(brand_grouping_name)
    # print(">>>>>>>>>>>>>>>>> SUBSSUBS",subbrands_list)
    return render(request, 'brands/subbrands.html', {'group_name': brand_grouping_name, 'subbrands_list': subbrands_list, 'startdate':startdate, 'enddate':enddate})


def runwatch(request,job_details):
    cmd = job_details
    cs = run_remote_command(cmd)
    # cs = refresh_output(job_details)
    # (dynoid,hap) = job_details.split("__")
    # print(f"We are in runwatch....request={request} request_POST={request.POST} jobdetails={job_details}")
    csout = cs.stdout.split('\n')
    cserr = cs.stderr.split('\n')
    # if 'DBG_JS_DBS' in os.environ.keys():
    #     cserr.append(f"DBS =+= {os.environ['DBG_JS_DBS']} =+=")
    if len(csout)>2:
        # print("len biger than 0","***",csout[-1])
        # print("LAST LINE err",cserr[-2],"LAST LINE out",csout[-2])
        if csout[-1]=='': del csout[-1]
        if "State changed from up to complete" in csout[-1]:
            csout.append(" --- ")
            csout.append(" ----- ")
            csout.append("THIS JOB IS DONE.")
        if "State changed from up to complete" in cserr:
            cserr.append(" -E- ")
            cserr.append(" --E-- ")
            cserr.append("THIS JOB IS DONE.")
    # return HttpResponseRedirect(reverse('brands:index'))
    return render(request,'brands/finalresult.html',{'job_details':job_details,'csout':csout,'cserr':cserr})

def brand_searcher(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # print("form valid , what next?")
            # print("form = ",form.cleaned_data)
            searched = request.POST['searched']
            queried_brand_details = {}
            for e in Brand_Details.objects.all().using('brands'):
                # print("POST", e.brand_id, e.brand_display_name)
                srch = searched.lower()
                if srch in e.brand_display_name.lower() or srch in str(e.brand_id).lower() \
                        or srch in e.brand_grouping_id.lower() or srch in e.backend_interface_group.lower():
                    queried_brand_details[e.brand_grouping_id] = [e.brand_id, e.brand_display_name,
                                                                  e.brand_group_details_id,
                                                                  e.brand_grouping_id, e.backend_interface_group]
            # queried_brand_details = query with searched ( hint Brand_Details.objects.filter(brand_display_name__contains='searched' )
            return render(request, 'brands/brand_search_result.html',
                          {'searched': searched, 'queried_brand_details': queried_brand_details})
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()
    return render(request, 'brands/search_form.html', {'form': form})

def runwatch_api(request,job_details):
    cmd = job_details
    return render(request,'brands/finalresult.html',{'job_details':'job_details','csout':"THIS IS CSOUT",'cserr':'THIS IS CSERR'})

    cs = run_remote_command(cmd)
    # cs = refresh_output(job_details)
    # (dynoid,hap) = job_details.split("__")
    # print("We are in runwatch....")
    csout = cs.stdout.split('\n')
    cserr = cs.stderr.split('\n')
    # if 'DBG_JS_DBS' in os.environ.keys():
    #     cserr.append(f"DBS =+= {os.environ['DBG_JS_DBS']} =+=")
    return render(request,'brands/finalresult.html',{'job_details':'job_details','csout':csout,'cserr':cserr})
    if len(csout)>2:
        # print("len biger than 0","***",csout[-1])
        # print("LAST LINE err",cserr[-2],"LAST LINE out",csout[-2])
        if csout[-1]=='': del csout[-1]
        if "State changed from up to complete" in csout[-1]:
            csout.append(" --- ")
            csout.append(" ----- ")
            csout.append("THIS JOB IS DONE.")
        if "State changed from up to complete" in cserr:
            cserr.append(" -E- ")
            cserr.append(" --E-- ")
            cserr.append("THIS JOB IS DONE.")
    # return HttpResponseRedirect(reverse('brands:index'))
    return render(request,'brands/finalresult.html',{'job_details':'job_details','csout':csout,'cserr':cserr})

def results(request, brand_grouping_name):
    run_message = f"""{get_heroku_exe()} run:detached doherokupython run -a biz-report {brand_grouping_name}.py  --mtd """
    return render(request, 'brands/results.html', {'running_command': run_message})

def running_processes(request):
    pids = []
    PROCNAMES = ["python3"]
    EXCLUDE_PROCNAMES = ["networkd-dispatcher", "unattended-upgrades", "manage.py"]
    for proc in psutil.process_iter():
        pInfoDict = proc.as_dict()
        excludeit = False
        for exc in EXCLUDE_PROCNAMES:
            if exc in pInfoDict['cmdline']: excludeit = True
        if excludeit: continue
        for PROCNAME in PROCNAMES:
            if os.path.basename(__file__) in pInfoDict['cmdline']: continue
            # if "run.py" not in pInfoDict['cmdline']: continue
            if PROCNAME in proc.name():
                # print(f"{pInfoDict['username']},ppid={pInfoDict['ppid']}, pid={pInfoDict['pid']},{pInfoDict['status']}\n{pInfoDict['cmdline']}\n\n")
                pids.append(f"pid={pInfoDict['pid']} {str(pInfoDict['cmdline'])}")
    if len(pids)>0:
        pids.append("--")
        pids.append("--")
        pids.append(f"Updated on {datetime.datetime.now()}")
        touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}running_processes.usedon")
    return render(request, 'brands/running_processes.html', {'pids': pids})

def cron_jobs(request):
    # print("cron_jobs", request , *request)
    cronlines = []
    cmd = ['crontab','-l']
    cs = subprocess.run(cmd, shell=False, check=True, timeout=1200, capture_output=True,
                        universal_newlines=True)  # , stdout=subprocess.PIPE ,    print(end='')
    for oneline in cs.stdout.split("\n"):
        oneline = oneline.strip()
        if "dom mon dow" in oneline: cronlines.append(f"{oneline}________________# (now time: {datetime.datetime.now()}")
        elif not oneline.startswith("#") : cronlines.append(oneline)
    return render(request, 'brands/cron_jobs.html', {'cronlines': cronlines})

def last_legend_log(request):
    # print("last_log_mtd", request , *request)
    ignored = []  #  get_ignored_terms()
    fn=is_it_newest(folder=os.environ['RUN_LEGEND_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
    loglines.append("--")
    loglines.append("--")
    crontab_lines = get_crontab_lines(forwhat="last_legend_cron")
    for crl in crontab_lines: loglines.append(crl)
    return render(request, 'brands/last_legend_log.html', {'loglines': loglines})

def last_8_log(request):
    # print("last_log_mtd", request , *request)
    ignored = get_ignored_terms()
    fn=is_it_newest(folder=os.environ['RUN_LAST8_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
    loglines.append("--")
    loglines.append("--")
    crontab_lines = get_crontab_lines(forwhat="last8_cron")
    for crl in crontab_lines: loglines.append(crl)
    return render(request, 'brands/last_8_log.html', {'loglines': loglines})

def last_mtd_log(request):
    # print("last_log_mtd", request , *request)
    ignored = get_ignored_terms()
    fn=is_it_newest(folder=os.environ['RUN_MTD_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
        # touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}last_mtd_log.usedon")
    loglines.append("--")
    loglines.append("--")
    crontab_lines = get_crontab_lines(forwhat="mtd_cron")
    for crl in crontab_lines: loglines.append(crl)

    return render(request, 'brands/last_mtd_log.html', {'loglines': loglines})

def last_prev_log(request):
    # print("last_log_mtd", request , *request)
    ignored = get_ignored_terms()
    fn=is_it_newest(folder=os.environ['RUN_PREV_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
        # touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}last_mtd_log.usedon")
    return render(request, 'brands/last_prev_log.html', {'loglines': loglines})

def last_8_full_log(request):
    # print("last_log_mtd", request , *request)
    ignored = []
    fn=is_it_newest(folder=os.environ['RUN_LAST8_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
    # loglines += get_crontab_lines(forwhat="last_cron_")
    return render(request, 'brands/last_8_log.html', {'loglines': loglines})

def last_mtd_full_log(request):
    # print("last_log_mtd", request , *request)
    ignored = []
    fn=is_it_newest(folder=os.environ['RUN_MTD_FOLDER'])
    loglines = 'THERE WERE NO LINES ACTUALLY'
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
        # touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}last_mtd_log.usedon")
    return render(request, 'brands/last_mtd_log.html', {'loglines': loglines})

def last_prev_full_log(request):
    # print("last_log_mtd", request , *request)
    ignored = []
    fn=is_it_newest(folder=os.environ['RUN_PREV_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
        # touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}last_mtd_log.usedon")
    return render(request, 'brands/last_prev_log.html', {'loglines': loglines})

def last_saas_log(request):
    # print("last_log_mtd", request , *request)
    ignored = get_ignored_terms()
    fn=is_it_newest(folder=os.environ['RUN_SAAS_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
        # touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}last_mtd_log.usedon")
    return render(request, 'brands/last_saas_log.html', {'loglines': loglines})

def last_data_collector_log(request):
    # print("last_log_mtd", request , *request)
    ignored = get_ignored_terms()
    fn=is_it_newest(folder=os.environ['RUN_DATA_COLLECTOR_FOLDER'])
    loglines = ''
    if fn is not None:
        loglines = show_log(fn=fn, ignored=ignored)
        # touch_file(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}last_mtd_log.usedon")
    return render(request, 'brands/last_data_collector_log.html', {'loglines': loglines})

def get_crontab_lines(forwhat=''):
    cronlines = []
    cmd = ['crontab','-l']
    cs = subprocess.run(cmd, shell=False, check=True, timeout=1200, capture_output=True,
                        universal_newlines=True)  # , stdout=subprocess.PIPE ,    print(end='')
    for oneline in cs.stdout.split("\n"):
        oneline = oneline.strip()
        if forwhat in oneline: cronlines.append(f"{oneline}")
    print(*cronlines)
    return cronlines

def build_cmd(request, form, brand_grouping_name):
    from_date=None
    to_date = None
    print("-=--=-=-=-=-=-=-=",os.getcwd())
    # frh = open('auto-run-one_command_template', 'r');alllines = frh.readlines();frh.close()
    template_lines = """#!/bin/bash
#set -x
cd ${OPTIVAL_TOOLS_MASTER}
. venv/bin/activate
. ${HOME}/.prod_db
cd ${OPTIVAL_TOOLS_MASTER_PYTHON}
#echo "Virtual env folder is" $VIRTUAL_ENV
PYTHON_ARGS_LINE
"""
    alllines = template_lines.split("\n")
    alllines = ['#!/bin/bash', '#set -x', 'cd ${OPTIVAL_TOOLS_MASTER}',
                '. venv/bin/activate', '. ${HOME}/.prod_db', 'cd ${OPTIVAL_TOOLS_MASTER_PYTHON}',
                '#echo "Virtual env folder is" $VIRTUAL_ENV', 'PYTHON_ARGS_LINE']
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-one_command_{mytimestamp}.sh"
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    accumulated_do_subs=''
    extra_switches=''
    with open(fn, 'w') as fhw:
        for row in alllines:
            # print("alllines row",row)
            if row=="PYTHON_ARGS_LINE":
                print("python3 run.py ",end='',file=fhw)
                # print(f"""--brand {brand_grouping_name} """,end='',file=fhw)
                # print(f"cmd2 is {cmd}")
                static_names = {'backend_data':"no_backend",'business_reporting':'no_biz_rep',
                                'gaming':'no_gaming','moreswitches':'More switches'}
                # print(f"form = {form}")
                for mykey in form.keys():
                    if mykey == "csrfmiddlewaretoken" : continue
                    if mykey in static_names:
                        # the logic is reversd logic . If mykey exists in form it means it is checked and needed.
                        # therefore for statics its switch should not be implemented.
                        # print("STATICNAMES",mykey,form[mykey])
                        if mykey == "moreswitches" : extra_switches = form[mykey]
                        del static_names[mykey]
                        # print(f"{mykey} deleted from static names")
                        continue
                    mydate = ''
                    if 'StartDate' not in form.keys() or 'EndDate' not in form.keys() : return None
                    if len(form['StartDate']) == 0 or len(form['EndDate']) == 0  : return None
                    if 'StartDate' == mykey :
                        try:
                            mydate  = form['StartDate'][0:form['StartDate'].index('T')]
                        except:
                            pass
                        try:
                            testdate=datetime.datetime.strptime(mydate,"%Y-%m-%d")
                            print(f"""--from_date={mydate} """,end='',file=fhw)
                            # print(f"cmd4 is {cmd}")
                            from_date = testdate
                        except:
                            return None
                        continue
                    if 'EndDate' == mykey :
                        try:
                            mydate  = form['EndDate'][0:form['EndDate'].index('T')]
                        except:
                            pass
                        try:
                            testdate=datetime.datetime.strptime(mydate,"%Y-%m-%d")
                            print(f"""--to_date={mydate} """,end='',file=fhw)
                            # print(f"cmd5 is {cmd}")
                            to_date = testdate
                        except:
                            return None
                        continue
                # this is remaining items
                    onebrid=f'--brand_id={mykey[mykey.index("|")+1:]} '
                    print(onebrid,end='',file=fhw)
                    # print(f'{onebrid},x')
                    accumulated_do_subs += mykey+","
            # print(f"cmd6 is {cmd}")

                # print(f"leftwith static names = {static_names}")
                for onestatic in static_names.keys():
                    print(f"""--{static_names[onestatic]} """,end='',file=fhw)

                if extra_switches != '' : print(f"{extra_switches} ",end='',file=fhw)
                    # print(f"cmd{onestatic} is {cmd}")
                if from_date>to_date : return None
                if accumulated_do_subs == '' : print(f"""--brand={brand_grouping_name} """,end='',file=fhw)
                print(file=fhw)
                # print(f"# {accumulated_do_subs}\n# {brand_grouping_name}", file=fhw)
            else:
                print(row,file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG']=='True' : print(f"brands/views/build_cmd Will run this command = {autorun_file}")
    return autorun_file

def build_post_cmd(request, form):
    from_date=None
    to_date = None

    alllines = ['#!/bin/bash', '#set -x', 'cd ${OPTIVAL_TOOLS_MASTER}',
                '. venv/bin/activate', '. ${HOME}/.prod_db', 'cd ${OPTIVAL_TOOLS_MASTER_PYTHON}',
                '#echo "Virtual env folder is" $VIRTUAL_ENV', 'PYTHON_ARGS_LINE']
    form = {'backend_data':"x",'business_reporting':'x','gaming':'x',"888 Mobile":'x'}
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-one_command_{mytimestamp}.sh"
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    with open(fn, 'w') as fhw:
        for row in alllines:
            # print("alllines",row)
            if row=="PYTHON_ARGS_LINE":
                print("python3 run.py ",end='',file=fhw)

                # THIS NEEDS TO BE REPLACED print(f"""--brand {brand_grouping_name} """,end='',file=fhw)

                static_names = {'backend_data':"no_backend",'business_reporting':'no_biz_rep','gaming':'no_gaming'}
                # print(f"form = {form}")
                for mykey in form.keys():
                    if mykey == "csrfmiddlewaretoken" : continue
                    if mykey in static_names:
                        # the logic is reversd logic . If mykey exists in form it means it is checked and needed.
                        # therefore for statics its switch should not be implemented.
                        del static_names[mykey]
                        # print(f"{mykey} deleted from static names")
                        continue
                    mydate = ''

                    # THIS NEEDS TO BE REPLACED if 'StartDate' not in form.keys() or 'EndDate' not in form.keys() : return None
                    # THIS NEEDS TO BE REPLACED if len(form['StartDate']) == 0 or len(form['EndDate']) == 0  : return None


                    if 'StartDate' == mykey :
                        try:
                            mydate  = form['StartDate'][0:form['StartDate'].index('T')]
                        except:
                            pass
                        try:
                            testdate=datetime.datetime.strptime(mydate,"%Y-%m-%d")
                            print(f"""--from_date={mydate} """,end='',file=fhw)
                            # print(f"cmd4 is {cmd}")
                            from_date = testdate
                        except:
                            return None
                        continue
                    if 'EndDate' == mykey :
                        try:
                            mydate  = form['EndDate'][0:form['EndDate'].index('T')]
                        except:
                            pass
                        try:
                            testdate=datetime.datetime.strptime(mydate,"%Y-%m-%d")
                            print(f"""--to_date={mydate} """,end='',file=fhw)
                            # print(f"cmd5 is {cmd}")
                            to_date = testdate
                        except:
                            return None
                        continue
                # this is remaining items
                    print(f'--do_subs="{mykey}" ',end='',file=fhw)
            # print(f"cmd6 is {cmd}")

                # print(f"leftwith static names = {static_names}")
                for onestatic in static_names.keys():
                    print(f"""--{static_names[onestatic]} """,end='',file=fhw)
                    # print(f"cmd{onestatic} is {cmd}")
                if from_date>to_date : return None
                print(file=fhw)
            else:
                print(row,end='',file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG']=='True' :
        print(f"brands/views/build_post_cmd Will run this command = {autorun_file}")
    return autorun_file

# def refresh_output(job_details):
#     (dyno,h_app) = job_details.split("__")
#     cmd = ['heroku','logs','--app', h_app, '--dyno', dyno, '-n', '100000']
#     # print(cmd)
#     if 'DATABASE_URL' in os.environ.keys() and "THIS_IS_HEROKU_PLATFORM" in os.environ.keys():
#         mydate = str
#         cmd = ["echo","weareinherokuguifromwhichwecannotlaunchautorun",f"{datetime.datetime.now()}"]
#     cs = subprocess.run(cmd, shell=False, check=True, timeout=1200, capture_output=True,
#                         universal_newlines=True)  # , stdout=subprocess.PIPE ,    print(end='')
#     return cs


