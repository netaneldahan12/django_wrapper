import os

import redis
from rq import Worker, Queue, Connection

import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE','django_wrapper.settings')
django.setup()


listen = ['high', 'default', 'low']
if "THIS_IS_HEROKU_APP" in os.environ.keys():
    conn = redis.from_url(os.environ.get("REDIS_URL"))
else:
    redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
    conn = redis.from_url(redis_url)

print("REDIS connection=",conn)


if __name__ == '__main__':
    print("worker conn",conn)
    with Connection(conn):
        worker = Worker(map(Queue, listen))
        print("owrker",worker)
        worker.work()

