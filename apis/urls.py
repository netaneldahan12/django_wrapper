from django.urls import path

from . import views

app_name = 'apis'
urlpatterns = [
    path('', views.run_by_post, name='run_by_post'),
    path('run_brand', views.run_brand_by_post, name='run_brand_by_post'),
    path('run_brand_no_capture', views.run_brand_by_post_no_capture, name='run_brand_by_post_no_capture'),
    path('run_brand_all_logs', views.run_brand_all_logs_by_post, name='run_brand_all_logs_by_post'),
    path('seoanalytics', views.seo_by_post, name='seo_by_post'),
    path('argosubmitter', views.argosubmitter_by_post, name='argosubmitter_by_post'),
    path('podlog', views.podlog_by_post, name='podlog_by_post'),
    path('uploadconversions', views.conversions_by_post, name='conversions_by_post'),
    path('upconverftd', views.conversions_by_post_ftd, name='conversions_by_post_ftd'),
    path('upconverqftd', views.conversions_by_post_qftd, name='conversions_by_post_qftd'),
    path('upconversignup', views.conversions_by_post_signup, name='conversions_by_post_signup'),
    path('upconverqftdnaiveroas', views.conversions_by_post_qftdnaiveroas, name='conversions_by_post_qftdnaiveroas'),
    path('test_automate', views.test_automate, name='test_automate'),
    path('abtest_snapshot', views.abtest_snapshot, name='abtest_snapshot'),
    path('run_data_collector', views.run_data_collector_by_post, name='run_data_collector_by_post'),
    # path('api/br/<int:brand_id>', views.run_by_post, name="brand_run_by_post"),
    # path('runwatch/<str:job_details>', views.runwatch, name='runwatch')
]