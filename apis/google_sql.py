import os
import sys

import pandas as pd
import pymysql



class GoogleSQL:
    def __init__(self):
        current_dir = f"{os.path.dirname(os.path.abspath(__file__))}"
        parentpath = f"{os.path.abspath(os.path.join(current_dir, os.pardir))}"
        self.connection = pymysql.connect(
            host=os.getenv('DB_HOST', None),
            port=int(os.getenv('DB_PORT', None)),
            user=os.getenv('DB_USER', None),
            passwd=os.getenv('DB_PASS', None),
            db=os.getenv('DB_NAME', None),
            ssl_key=f'{parentpath}/credentials/client-key.pem',
            ssl_cert=f'{parentpath}/credentials/client-cert.pem',
            ssl_ca=f'{parentpath}/credentials/server-ca.pem'
        )

    def insert_bulk(self, query: str, values: list[tuple]) -> None:
        try:
            with self.connection.cursor() as cursor:
                cursor.executemany(query, values)
                self.connection.commit()
                cursor.close()
        except Exception as ex:
            raise ex

    def insert(self, query: str, values: tuple[str, ...]) -> None:
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(query, values)
                self.connection.commit()
                cursor.close()
        except Exception as ex:
            raise ex

    def read_query(self, query:str ):
        try:
            with self.connection.cursor() as cursor:
                result = cursor.execute(query)
                recordrows = cursor.fetchall()
                return recordrows
        except Exception as ex:
            raise ex
        finally:
            cursor.close()

    def get_query_as_dataframe(self, query:str ):
        try:
            with self.connection.cursor() as cursor:
                result = cursor.execute(query)
                recordrows = cursor.fetchall()
                row_headers = [x[0] for x in cursor.description]  # this will extract row headers
                json_data = []
                for result in recordrows: json_data.append(dict(zip(row_headers, result)))
                return pd.DataFrame.from_records(json_data)
        except Exception as ex:
            raise ex
        finally:
            cursor.close()
            self.close_connection()

    def get_query_as_json(self, query:str ):
        try:
            with self.connection.cursor() as cursor:
                result = cursor.execute(query)
                recordrows = cursor.fetchall()
                row_headers = [x[0] for x in cursor.description]  # this will extract row headers
                json_data = []
                for result in recordrows: json_data.append(dict(zip(row_headers, result)))
                return json_data
        except Exception as ex:
            raise ex
        finally:
            cursor.close()
            self.close_connection()

    def close_connection(self) -> None:
        if self.connection:
            self.connection.close()
