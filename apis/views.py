import datetime
import json
import re
from datetime import timezone
import stat
import sys
import time
import os
import subprocess
import traceback
import shutil

import google
from google.oauth2 import service_account
import google.auth.transport.requests


import requests
from django.shortcuts import get_object_or_404,render,HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.http import JsonResponse
from rest_framework import status

# from .models import Brand_Details
# from .models import Brand_Grouping
# from .utils_copy import *

from rest_framework.decorators import api_view, authentication_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

currentdir= f"{os.path.dirname(os.path.abspath(__file__))}"
parentdir = f"{os.path.abspath(os.path.join(currentdir, os.pardir))}{os.sep}"
sys.path.append(parentdir)
sys.path.append(currentdir)
import google_sql
from . import bucket_uploader
# from uploader import GoogleStorage


@api_view(['POST'])
def seo_by_post(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys(): return Response(status=401)
        if not is_token_valid(request.POST['token'],whichone="SEO_API_TOKEN"): return Response(status=401)

        autorunfile = build_seo_cmd(request, request.POST)

        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        # print(f"Going Into remote command {autorunfile} {request.POST} on {os.getcwd()} {datetime.datetime.now()}")

        cs = run_remote_command(autorunfile,kind='seo')
        txt=''
        stats_file_name=''
        stats_json={'CREATED':0, "TOTAL FETCHED":0, "UPDATED":0,"message":txt}
        for line in cs.stdout.split("\n"):
            txt+=line+"\n"
            if line.startswith("THIS_SESSION_SEO_STATS_FILE"):
                junk,stats_file_name=line.split('=')
                # print(stats_file_name)
        for line in cs.stderr.split("\n"): txt+=line+"\n"
        if os.path.exists(stats_file_name):
            try:
                with open(stats_file_name,"r") as fhr:
                    aa = ["TOTAL FETCHED", "UPDATED","CREATED"]
                    for statline in fhr.readlines():
                        statline = statline.strip().strip("\s")
                        for item in aa:
                            if statline.startswith(item):
                                fld,val = statline.split("=")
                                stats_json[item] = val
                with open (f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}SEO_OUT_DEBUG_FILE.txt","w") as outf:
                    print("--",txt,"==",file=outf)
                    print("--", txt, "==")
                stats_json["message"] += txt
            except:
                print("ERROR something went wrong wih conversions process")
        print(stats_json,file=sys.stderr)
        return JsonResponse(stats_json)
        # return Response("just a string")

    print("should not be here")
    return None


@api_view(['POST'])
def run_by_post(request):
    if request.method=="POST":
        # print("is post")
        # print(request.POST)
        if not 'token' in request.POST.keys(): return Response(["returncode=401"])
        if not is_token_valid(request.POST['token']): return Response(["returncode=401"])
        # del request.POST['token']

        autorunfile = build_manadj_cmd(request, request.POST)

        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        cs = run_remote_command(autorunfile,kind='manual_adjustment')
        # print(cs.returncode)
        # print(cs.stdout)
        # print(cs.stderr)
        if 'MANADJERROR' in cs.stdout or 'MANADJERROR' in cs.stderr:
            response_obj = [f"returncode=1 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        elif "ERROR" in cs.stdout or "ERROR" in cs.stderr:
            response_obj = [f"returncode=-1 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        else:
            response_obj = ["returncode=0"]
        # print(response_obj)
        return Response(response_obj)
    print("should not be here")
    return None

@api_view(['POST'])
def run_data_collector_by_post(request):
    if request.method=="POST":
        # print("is post")
        # print(request.POST)
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print("DEBUG is post")
            print("DEBUG",request.POST)

        if not 'token' in request.POST.keys(): return Response(["returncode=401"])
        if not is_token_valid(request.POST['token'],whichone="DATA_COLLECTOR_API_TOKEN"): return Response(["returncode=401"])
        # del request.POST['token']

        autorunfile = build_data_collector_cmd(request, request.POST)

        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        cs = run_remote_command(autorunfile,kind='data_collector')
        # print(cs.returncode)
        # print(cs.stdout)
        # print(cs.stderr)
        response_obj = [f"returncode=0 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()} || {request.user}"]
        response_obj = {"returncode":0 , 'stdout':cs.stdout.strip().splitlines() , 'stderr':cs.stderr.strip().splitlines() }
        # print(response_obj)
        return JsonResponse(response_obj)
    print("should not be here")
    return None

@api_view(['POST'])
def run_brand_by_post(request):
    if request.method == "POST":
        if not 'token' in request.POST.keys(): return Response(["returncode=401"])
        if not is_token_valid(request.POST['token'],whichone="BRAND_RUN_TOKEN"): return Response(["returncode=401"])
        # del request.POST['token']

        autorunfile = build_brand_run_cmd(request, request.POST)

        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        cs = run_remote_command(autorunfile,kind='run_brand')
        # print(cs.returncode)
        # print(cs.stdout)
        # print(cs.stderr)
        if 'MANADJERROR' in cs.stdout or 'MANADJERROR' in cs.stderr:
            response_obj = [f"returncode=1 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        elif "ERROR" in cs.stdout or "ERROR" in cs.stderr:
            response_obj = [f"returncode=-1 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        elif "usage:" in cs.stdout or "usage:" in cs.stderr:
            response_obj = [f"returncode=-1 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        else:
            response_obj = [f"returncode=0"]
        # print(response_obj)
        return Response(response_obj)
    print("should not be here")
    return None

@api_view(['POST'])
def run_brand_by_post_no_capture(request):
    if request.method == "POST":
        if not 'token' in request.POST.keys(): return Response(["returncode=401"])
        if not is_token_valid(request.POST['token'],whichone="BRAND_RUN_TOKEN"): return Response(["returncode=401"])
        # del request.POST['token']

        autorunfile = build_brand_run_cmd(request, request.POST)

        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        mytimestamp = str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.', '')
        logfilepath = f"/tmp/django_wrapper_{os.getpid()}_{mytimestamp}_run_brand_no_capture.log"
        cs = run_remote_command_no_capture(autorunfile,kind='run_brand_no_capture',logfilepath=logfilepath)

        with (open(logfilepath, "r") as logfile):
            # for last_line in logfile:
            #     print(last_line, "before some sleep")
            # # time.sleep(0.5)
            # else:
            #     data = cs.communicate()
            #     cs.terminate()
            loglines = logfile.readlines()
            pattern=re.compile(r'\n')
            loglines = [pattern.sub(r'<br>\n', sub) for sub in loglines]

        response_obj = [f"returncode=0 || {loglines}"]
        # print(response_obj)
        return Response(response_obj)
    print("should not be here")
    return None

@api_view(['POST'])
def run_brand_all_logs_by_post(request):
    if request.method == "POST":
        if not 'token' in request.POST.keys(): return Response(["returncode=401 no token"])
        if not is_token_valid(request.POST['token'],whichone="BRAND_RUN_TOKEN"): return Response([f"""returncode=401 wrong token {request.POST['token']}"""])
        # del request.POST['token']

        autorunfile = build_brand_run_cmd(request, request.POST)

        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        cs = run_remote_command(autorunfile,kind='run_brand')
        # print(cs.returncode)
        # print(cs.stdout)
        # print(cs.stderr)

        response_obj = [{'returncode':'0'} , {'stdout':cs.stdout.split("\n")}, {'stderr': cs.stderr.split("\n")}]
        # print(response_obj)
        return Response(response_obj)
    print("should not be here")
    return None



def to_bucket(path:str):
    gs = bucket_uploader.GoogleStorage()
    destination_path = f"{datetime.datetime.now().year}/{datetime.datetime.now().month}/{os.path.basename(path)}"
    url = gs.upload_to_bucket(destination_path=destination_path, source_file=path)
    return url



def run_the_request(convkind=''):
    autorunfile = build_conversions_cmd(convkind=convkind)

    if autorunfile is None:
        brand_grouping_name = "XXX"
        print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
        return Response([], status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    # print(f"Going Into remote command {autorunfile} {request.POST} on {os.getcwd()} {datetime.datetime.now()}")

    cs = run_remote_command(autorunfile,kind='conversions')
    txt=''
    alltxt = ''
    stats_file_name=''
    stats_json={'CREATED':0,"TOTAL FETCHED":-1, "UPDATED":-1}

    try:
        cs.stdout
        for line in cs.stdout.split("\n"):
            alltxt += line+"\n"
            if "Ads_Uploader_Exit_Status" in line:
                txt+=line+"<br>\n"
            if line.startswith("THIS_SESSION_CONVERSIONS_STATS_FILE"):
                junk,stats_file_name=line.split('=')
                # print(stats_file_name)
    except:
        print(f"Could not reach stdout for {autorunfile}")
        return Response({["returncode=1", f"autorunfile unreachable {autorunfile}"]},
                        exception=True, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    try:
        for line in cs.stderr.split("\n"):
            txt+=line+"<br>\n"
            alltxt += line+"\n"
        if os.path.exists(stats_file_name):
            try:
                with open(stats_file_name,"r") as fhr:
                    aa = ["TOTAL FETCHED", "UPDATED"]
                    for statline in fhr.readlines():
                        statline = statline.strip().strip("\s")
                        for item in aa:
                            if statline.startswith(item):
                                fld,val = statline.split("=")
                                stats_json[item] = val
                                # print(f"{item=},{val=}")
                stats_json["message"] = txt+f"\nreturncode={cs.returncode}\n"
            except:
                print("ERROR something went wrong with stats writing")
                return Response({["returncode=1", f"something went wrong wih conversions process"]},
                                exception=True, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    except:
        print(f"Could not reach stderr for {autorunfile}")
    datestring = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    try:
        with open(f"/tmp/run_conv_bucket_file_{datestring}.log", "w") as wfile:
            print(alltxt, file=wfile)
        bucket_url = to_bucket(path=f"/tmp/run_conv_bucket_file_{datestring}.log")
        stats_json['message'] += "Log can be found on:"+f"https://console.cloud.google.com/storage/browser/conversion_logs/{datetime.datetime.now().year}/{datetime.datetime.now().month}\n\n"+bucket_url+"\n"
        stats_json['log_url'] = bucket_url
    except Exception as ex:
        print("ERROR could not create google bucket file")
        raise(ex)
    # print("\n\nSTATS_JSON", stats_json,"-=-=-=-=-=-=--=XXXX-=-=-=-=--\n\n")
    return Response(stats_json, status=status.HTTP_200_OK)
    # return Response( {'reason': 'ok',  'data':stats_json}, status=status.HTTP_200_OK)
    # return JsonResponse(stats_json)
    # return Response("just a string")


@api_view(['POST'])
def conversions_by_post(request,convkind=''):
    if request.method=="POST":
        if not 'token' in request.POST.keys(): return Response(status=401)
        if not is_token_valid(request.POST['token'],whichone="CONV_API_TOKEN"): return Response(status=401)

        return run_the_request(convkind=convkind)

    print("should not be here")
    return None

@api_view(['POST'])
def conversions_by_post_ftd(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys(): return Response(status=401)
        if not is_token_valid(request.POST['token'],whichone="CONV_API_TOKEN"): return Response(status=401)

        return run_the_request(convkind="ftd")

    print("should not be here")
    return None

@api_view(['POST'])
def conversions_by_post_qftd(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys(): return Response(status=401)
        if not is_token_valid(request.POST['token'],whichone="CONV_API_TOKEN"): return Response(status=401)

        return run_the_request(convkind="qftd")

    print("should not be here")
    return None

@api_view(['POST'])
def conversions_by_post_signup(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys(): return Response(status=401)
        if not is_token_valid(request.POST['token'],whichone="CONV_API_TOKEN"): return Response(status=401)

        return run_the_request(convkind="signup")

    print("should not be here")
    return None

@api_view(['POST'])
def conversions_by_post_qftdnaiveroas(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys(): return Response(status=401)
        if not is_token_valid(request.POST['token'],whichone="CONV_API_TOKEN"): return Response(status=401)

        return run_the_request(convkind="qftdnaiveroas")

    print("should not be here")
    return None


@api_view(['POST'])
def test_automate(request):
    if request.method == "POST":
        if not 'token' in request.POST.keys(): return Response(["returncode=401"])
        if not is_token_valid(request.POST['token'],whichone="TEST_AUTOMATE_TOKEN"): return Response(["returncode=401"])
        # del request.POST['token']

        autorunfile = os.environ['OPTIVAL_ROOT']+os.sep+"optival-automation"+os.sep+"run_automation_ubuntu.sh"
        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        cs = ''
        try:
            if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
                print(f"Starting {autorunfile} on {os.getcwd()} {datetime.datetime.now()}")
            cs = subprocess.run(autorunfile, check=True, timeout=2400, capture_output=True,
                                universal_newlines=True)  # , stdout=subprocess.PIPE ,    print(end='')
        except:
            response_obj = [f"returncode=1 ||  stderr={traceback.format_exc()}"]
            print(response_obj,file=sys.stderr)
            write_response_log(response_obj,"test_automate_failed")
            return Response(response_obj)
        response_obj = [f"returncode=0 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        write_response_log(response_obj, "test_automate_success")
        return Response(response_obj)
    print("should not be here")
    return None

@api_view(['POST'])
def abtest_snapshot(request):
    if request.method == "POST":
        if not 'token' in request.POST.keys(): return Response(["returncode=401a"])
        if not is_token_valid(request.POST['token'],whichone="ABTEST_SNAPSHOT_TOKEN"): return Response(["returncode=401b"])
        # del request.POST['token']
        with open("/home/ubuntu/Downloads/abtest_snapshot.log","w") as logfile:
            print(request.POST,file=logfile)
            # print(request.POST['url'],file=logfile)
        datestring = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
        try:
            with open(f"/tmp/run_abtest_snapshot_{datestring}.sh","w") as wfile:
                print("#!/usr/bin/bash",file=wfile)
                print(f"cd {os.environ['OPTIVAL_REPORTING_ROOT']}",file=wfile)
                print(". env_setup_for_master",file=wfile)
                try:    print(f"python3 abtest_snapshot.py url={request.POST['url']}",end=' ', file=wfile)
                except:
                    with open("/home/ubuntu/Downloads/abtest_snapshot.log", "a") as logfile:
                        print(f"useless post without url", file=logfile)
                try:               print(f"rpt={request.POST['rpt']} ",end=' ', file=wfile)
                except: pass
                try:               print(f"intrvl={request.POST['intrvl']} ",end=' ', file=wfile)
                except: pass
                try:               print(f"dev={request.POST['dev']} ",end=' ', file=wfile)
                except: pass
                print(file=wfile)
        except:
            with open("/home/ubuntu/Downloads/abtest_snapshot.log", "a") as logfile:
                print(traceback.format_exc(), file=logfile)
        autorunfile = f"/tmp/run_abtest_snapshot_{datestring}.sh"
        st = os.stat(autorunfile)
        os.chmod(autorunfile, st.st_mode | stat.S_IEXEC)
        if autorunfile is None:
            brand_grouping_name = "XXX"
            print(">>>>>>>>>>>>>>>> CMD WAS NONE ???????")
            return Response(["empty array"])
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Going Into remote command {autorunfile} ______________ on {os.getcwd()} {datetime.datetime.now()}")
        cs = ''
        try:
            if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
                print(f"Starting {autorunfile} on {os.getcwd()} {datetime.datetime.now()}")
            cs = subprocess.run(autorunfile, check=True, timeout=2400, capture_output=True,
                                universal_newlines=True)  # , stdout=subprocess.PIPE ,    print(end='')
            # os.remove(autorunfile)
        except:
            response_obj = [f"returncode=1 ||  stderr={traceback.format_exc()}"]
            print(response_obj,file=sys.stderr)
            write_response_log(response_obj,"abtest_snapshot_failed")
            # os.remove(autorunfile)
            return Response(response_obj)
        response_obj = [f"returncode=0 || stdout={cs.stdout.strip()} || stderr={cs.stderr.strip()}"]
        write_response_log(response_obj, "abtest_snapshot_success")
        return Response(response_obj)
    print("should not be here")
    return None

def write_response_log(response,kind):
    # print("kind is", kind)
    with open(f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{kind}{os.getpid()}.DONE", "w") as logfile:
        print(response, file=logfile)
    return 0


def run_remote_command(autorun_file,kind):
    cmdfullpath = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    os.environ.setdefault('ENV_AUTO_RUN_FILE_NAME_ENV', cmdfullpath)
    if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
        print(f"In remote command {cmdfullpath} ______________ on {os.getcwd()} {datetime.datetime.now()}")
    cs=None
    # orig_dir = os.getcwd()
    # os.chdir(os.environ['OPTIVAL_TOOLS_MASTER_PYTHON'])
    try:
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Starting {cmdfullpath} on {os.getcwd()} {datetime.datetime.now()}")
        if kind != "data_collector":
            cs = subprocess.run(cmdfullpath, shell=True,  check=True ,  timeout=2400 , capture_output=True,universal_newlines=True )  # , stdout=subprocess.PIPE ,    print(end='')
        else:
            cs = subprocess.run(cmdfullpath, shell=True,  check=True ,  timeout=2400 , capture_output=True,universal_newlines=True )  # , stdout=subprocess.PIPE ,    print(end='')
        # os.chdir(orig_dir)
        fhw = open(f"/tmp/django_wrapper_cs_info_{os.getpid()}_{kind}","w")
        try:
            print("returncode=",cs.returncode,file=fhw)
        except:
            return cs
        try:
            a = cs.stdout.split("\n")
        except:
            return cs
        for l in a: print(l,file=fhw)
        print("------- sep ------ stderr down here -----",file=fhw)
        try:
            a = cs.stderr.split("\n")
        except:
            return cs
        for l in a: print(l,file=fhw)
        fhw.close()
        with open(cmdfullpath,"a") as fhautorun:
            print(f"\t\t---- {kind} -----\n",file=fhautorun)
            print("\t\t----out-----\n",cs.stdout,file=fhautorun)
            print("\t\t----err-----\n",cs.stderr,file=fhautorun)

        with open(f"/tmp/django_wrapper_cs_info_{os.getpid()}_{kind}_raw","w") as rawf:
            print(cs.stdout,"\n\n",cs.stderr,file=rawf)

        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print("cs is=",cs,file=sys.stderr)
        new_cmd_file_path = cmdfullpath.replace('.sh','.DONE')
        os.rename(cmdfullpath,new_cmd_file_path)
        return cs
    except :
        print(f"ERROR {cmdfullpath} failed to execute ...... /tmp/django_wrapper_traceback .......................")
        fhw = open("/tmp/django_wrapper_traceback","w")
        # print(f"Failure cs returncode ={cs.returncode} in {os.getcwd()}",file=fhw)
        print(cs,file=fhw)
        print(traceback.format_exc(),file=fhw)
        print(traceback.print_stack(),file=fhw)
        fhw.close()
        # os.chdir(orig_dir)
        new_cmd_file_path = cmdfullpath.replace('.sh','.a.FAILED')
        os.rename(cmdfullpath,new_cmd_file_path)
        return None

def run_remote_command_no_capture(autorun_file,kind,logfilepath):
    cmdfullpath = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
        print(f"In remote command no_capture{cmdfullpath} ______________ on {os.getcwd()} {datetime.datetime.now()}")
    cs=None
    # orig_dir = os.getcwd()
    # os.chdir(os.environ['OPTIVAL_TOOLS_MASTER_PYTHON'])
    try:
        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print(f"Starting no_capture {cmdfullpath} on {os.getcwd()} {datetime.datetime.now()}")
        logfile=open(logfilepath, "w")
        cs = subprocess.run(cmdfullpath, shell=True , stderr=logfile, timeout=2400)  # , stdout=subprocess.PIPE ,    print(end='')
            # try:
            #     print("returncode=",cs.returncode,file=logfile)
            # except:
            #     return cs

        # with open(f"/tmp/django_wrapper_cs_info_{os.getpid()}_{kind}.log", "r") as logfile:
        #     for last_line in logfile:
        #         print(last_line, "before some sleep")
        #     # time.sleep(0.5)
        #     else:
        #         data = cs.communicate()
        #         cs.terminate()

        # with open(cmdfullpath,"a") as fhautorun:
        #     print(f"\t\t---- {kind}{kind} -----\n",file=fhautorun)
        #     print("\t\t----out-----\n",cs.stdout,file=fhautorun)
        #     print("\t\t----err-----\n",cs.stderr,file=fhautorun)

        with open(f"/tmp/django_wrapper_cs_info_{os.getpid()}_{kind}_raw","w") as rawf:
            print(cs.stdout,"\n\n",cs.stderr,file=rawf)

        if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
            print("cs is=",cs,file=sys.stderr)
        new_cmd_file_path = cmdfullpath.replace('.sh','.DONE')
        os.rename(cmdfullpath,new_cmd_file_path)
        return cs
    except :
        print(f"ERROR {cmdfullpath} failed to execute ...... /tmp/django_wrapper_traceback .......................")
        fhw = open("/tmp/django_wrapper_traceback","w")
        # print(f"Failure cs returncode ={cs.returncode} in {os.getcwd()}",file=fhw)
        print(cs,file=fhw)
        print(traceback.format_exc(),file=fhw)
        print(traceback.print_stack(),file=fhw)
        fhw.close()
        # os.chdir(orig_dir)
        new_cmd_file_path = cmdfullpath.replace('.sh','.b.FAILED')
        os.rename(cmdfullpath,new_cmd_file_path)
        return None

def build_manadj_cmd(request, form):
    # print(f"build_post_cmd request={request} form={form} {type(form)} {form.dict}" )
    # form_dict = form.dict()
    # for onedict in form_dict.keys():
    #     print(onedict,'==>',form_dict[onedict])
    # print(".......................................................")

    alllines = ['#!/bin/bash', '#set -x', 'cd ${OPTIVAL_TOOLS_MASTER}',
                '. venv/bin/activate', '. ${HOME}/.prod_db', 'cd ${OPTIVAL_TOOLS_MASTER_PYTHON}',
                '#echo "Virtual env folder is" $VIRTUAL_ENV', 'PYTHON_ARGS_LINE']
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-api_manadj_{mytimestamp}.sh"
    if not os.path.exists(os.environ['AUTO_RUNS_FOLDER']) : os.mkdir(os.environ['AUTO_RUNS_FOLDER'])
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    with open(fn, 'w') as fhw:
        for row in alllines:
            # print("alllines",row)
            if row=="PYTHON_ARGS_LINE":
                print("python3 run.py ",end='',file=fhw)

                for mykey in form.keys():
                    if mykey == "csrfmiddlewaretoken" : continue
                    if mykey == "token" : continue
                    if form[mykey]=='': print(f"--{mykey} ",end='',file=fhw)
                    else: print(f"--{mykey}={form[mykey]} ",end='',file=fhw)
                print(file=fhw)
            else:
                print(row,file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    # if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG']=='True' :
    # print(f"Will run this command = {fn}")
    # with open(fn,"r") as frw:
    #     ln = frw.readlines()
    #     print(ln,flush=True)
    return autorun_file

def build_data_collector_cmd(request, form):
    alllines = ['#!/bin/bash', '#set -x', 'cd ${OPTIVAL_TOOLS_MASTER}',
                '. venv/bin/activate', '. ${HOME}/.prod_db', 'cd ${OPTIVAL_TOOLS_MASTER_PYTHON}',
                '#echo "Virtual env folder is" $VIRTUAL_ENV', 'PYTHON_ARGS_LINE']
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-api_data_collector_{mytimestamp}.sh"
    if not os.path.exists(os.environ['AUTO_RUNS_FOLDER']) : os.mkdir(os.environ['AUTO_RUNS_FOLDER'])
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG'] == 'True':
        print("DEBUG", "file to write on",fn)
    with open(fn, 'w') as fhw:
        for row in alllines:
            # print("alllines",row)
            if row=="PYTHON_ARGS_LINE":
                print("python3 dc_run.py ",end='',file=fhw)

                for mykey in form.keys():
                    if mykey == "csrfmiddlewaretoken" : continue
                    if mykey == "token" : continue
                    if form[mykey]=='': print(f"--{mykey} ",end='',file=fhw)
                    else: print(f"--{mykey}={form[mykey]} ",end='',file=fhw)
                print(file=fhw)
            else:
                print(row,file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    return autorun_file

def build_brand_run_cmd(request, form):
    # print(f"build_post_cmd request={request} form={form} {type(form)} {form.dict}" )
    # form_dict = form.dict()
    # for onedict in form_dict.keys():
    #     print(onedict,'==>',form_dict[onedict])
    # print(".......................................................")

    alllines = ['#!/bin/bash', '#set -x', 'cd ${OPTIVAL_TOOLS_MASTER}',
                '. venv/bin/activate', '. ${HOME}/.prod_db', 'cd ${OPTIVAL_TOOLS_MASTER_PYTHON}',
                '#echo "Virtual env folder is" $VIRTUAL_ENV', 'PYTHON_ARGS_LINE']
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-api_brand_run_{mytimestamp}.sh"
    if not os.path.exists(os.environ['AUTO_RUNS_FOLDER']): os.mkdir(os.environ['AUTO_RUNS_FOLDER'])
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    with open(fn, 'w') as fhw:
        for row in alllines:
            # print("alllines",row)
            if row=="PYTHON_ARGS_LINE":
                print("python3 run.py ",end='',file=fhw)

                for mykey in form.keys():
                    if mykey == "csrfmiddlewaretoken" : continue
                    if mykey == "token" : continue
                    if form[mykey]=='': print(f"--{mykey} ",end='',file=fhw)
                    else: print(f"--{mykey}={form[mykey]} ",end='',file=fhw)
                print(file=fhw)
            else:
                print(row,file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    # if 'DJANGO_APP_DEBUG' in os.environ.keys() and os.environ['DJANGO_APP_DEBUG']=='True' :
    # print(f"Will run this command = {fn}")
    # with open(fn,"r") as frw:
    #     ln = frw.readlines()
    #     print(ln,flush=True)
    return autorun_file

def build_seo_cmd(request, form):
    alllines = ['#!/bin/bash', '#set -x', 'cd ${OPTIVAL_TOOLS_MASTER}',
                '. venv/bin/activate', '. ${HOME}/.prod_db', 'cd ${OPTIVAL_TOOLS_MASTER_PYTHON}',
                '#echo "Virtual env folder is" $VIRTUAL_ENV', 'PYTHON_ARGS_LINE']
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-api_seo_{mytimestamp}.sh"
    if not os.path.exists(os.environ['AUTO_RUNS_FOLDER']) : os.mkdir(os.environ['AUTO_RUNS_FOLDER'])
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    with open(fn, 'w') as fhw:
        # print(fn)
        for row in alllines:
            # print("alllines",row)
            if row=="PYTHON_ARGS_LINE":
                print("python3 seo_analytics.py ",end='',file=fhw)

                for mykey in form.keys():
                    if mykey == "csrfmiddlewaretoken" : continue
                    if mykey == "token" : continue
                    if form[mykey]=='': print(f"--{mykey} ",end='',file=fhw)
                    else: print(f"--{mykey}={form[mykey]} ",end='',file=fhw)
                print(file=fhw)
            else:
                print(row,file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    return autorun_file

def build_conversions_cmd(convkind):
    myscript = './dothis.sh'
    if convkind == 'ftd': myscript = "./doftdonly.sh"
    if convkind == 'qftd': myscript = "./doqftdonly.sh"
    if convkind == 'signup': myscript = "./dosignuponly.sh"
    if convkind == 'qftdnaiveoas': myscript = "./doqftdnaiveroasuponly.sh"
    alllines = ['#!/bin/bash', '#set -x', 'cd ${CONVERSIONS_UPLOAD_ROOT}', myscript,]
    mytimestamp=str(datetime.datetime.now().replace(tzinfo=timezone.utc).timestamp()).replace('.','')
    autorun_file = f"auto-run-api_conv_{mytimestamp}.sh"
    if not os.path.exists(os.environ['AUTO_RUNS_FOLDER']) : os.mkdir(os.environ['AUTO_RUNS_FOLDER'])
    fn = f"{os.environ['AUTO_RUNS_FOLDER']}{os.sep}{autorun_file}"
    with open(fn, 'w') as fhw:
        # print(fn)
        for row in alllines: print(row,file=fhw)
    os.chmod(fn,stat.S_IRWXU)
    return autorun_file

def is_token_valid(token,whichone="MANADJ_API_TOKEN"):
    if whichone in os.environ.keys():
        if str(token) == str(os.environ[whichone]):
            return True
    # with open(f"{os.environ['HOME']}.prod_db","r") as file:
    return False


@api_view(['POST'])
def argosubmitter_by_post(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys():
            return Response({"No token"}, status=status.HTTP_401_UNAUTHORIZED)
        if not is_token_valid(request.POST['token'],whichone="POD_API_TOKEN"):
            return Response({"Token not valid"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        post_data = dict(request.POST)
        access_token = "eyJhbGciOiJSUzI1NiIsImtpZCI6Imx5WEFmT1U5czE3RkEtNnM1TU0zM1VzaVlnV0hWZTd2OGx3X3RsUUJ1cjAifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJhcmdvIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImFyZ28uc2VydmljZS1hY2NvdW50LXRva2VuIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFyZ28iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJjODQ1N2NmYy0yMmQ0LTQ0MTktOGVkMy1kODJmN2QzMDViNGQiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6YXJnbzphcmdvIn0.CSueaI1MicuXB1RLXeHqDeGJbO5U3y-_Bpw_fW6lxoK3rGCOgxSHJEsIdK1GDQeBI89W054rcxKu36HPLLPUK-qX_PyY7N-RYdppjg6VE9TOqtFugvHmJ9x45VmcxhUzrqRR4JUwhgF5an_2bsc8CuO6hj6d_wHbWgBtRPrhtk3OcGJHur3QS4gvuZo7Yxf7biRMT3rcL3zd18lPz0s6zDVuphOJvICn1uK2J1jPNRvRzQClISfEQsuH_Rp5p4fBgerROweUphpcunHEarfauxerxBB-cSsOeTGAlhlsuYO2bgT1GkFvqt93U1oZFPnL0Y4ZXqxNL-HRAxnFZKOSlA"
        url = "https://34.39.21.224:2746/api/v1/workflows/argo/submit"
        headers = { "Content-Type": "application/json", "Authorization": f"Bearer {access_token}" }
        viewdf = google_sql.GoogleSQL().get_query_as_dataframe(f"select * from data_collector_view")
        onedf = viewdf[viewdf['raw_output_table_name'] == int(post_data['table_id'][0])]
        if onedf.shape[0] != 1:
            stats_json = { f"""Incorrect match for table_id {post_data['table_id'][0]}."""}
            return Response(stats_json, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        # print("df interface name ==> ",onedf['interface_name'].values[0].lower())
        payload = json.dumps({
            "resourceKind": "WorkflowTemplate",
            "resourceName": f"{onedf['interface_name'].values[0].lower()}-all",
            "namespace": "argo",
            "submitOptions": {
                "parameters": [
                    "pod_name=pod_name",
                    f"table_id={post_data['table_id'][0]}",
                    f"start_date={post_data['start_date'][0]}",
                    f"end_date={post_data['end_date'][0]}"
                ]
            }
        })

        response = requests.request("POST", url, headers=headers, data=payload, verify=False)
        jd = json.loads(response.text)
        pod_name = jd['metadata']['name']
        # print(f"{pod_name=}")
        loggerurl = f"""https://console.cloud.google.com/logs/query;"""
        # stats_json = {f"""https://console.cloud.google.com/logs/query;query=resource.labels.namespace_name%3D%22argo%22%0Aresource.labels.pod_name%3D~%22{pod_name}%22"""}
        # stats_json = {f"""{pod_name}\n{loggerurl}query=resource.labels.namespace_name%3D%22argo%22%0A-resource.labels.container_name%3D%22wait%22%0A-resource.labels.container_name%3D%22init%22%0Aresource.labels.pod_name%3D~%22{pod_name}%22"""}

        json_stats = {
            "pod_name": pod_name,
            "url": f"""{loggerurl}query=resource.labels.namespace_name%3D%22argo%22%0A-resource.labels.container_name%3D%22wait%22%0A-resource.labels.container_name%3D%22init%22%0Aresource.labels.pod_name%3D~%22{pod_name}%22"""
        }
        return JsonResponse(json_stats, json_dumps_params={'indent': 2})
    print("should not be here")
    return None

@api_view(['POST'])
def podlog_by_post(request):
    if request.method=="POST":
        if not 'token' in request.POST.keys():
            return Response({"No token"}, status=status.HTTP_401_UNAUTHORIZED)
        if not is_token_valid(request.POST['token'],whichone="POD_API_TOKEN"):
            return Response({"Token not valid"}, status=status.HTTP_406_NOT_ACCEPTABLE)

        post_data = dict(request.POST)
        if "pod_name" not in post_data.keys():
            return Response({ "Missing pod_name" }, status=status.HTTP_404_NOT_FOUND)
        lines = get_pod_name_log_info(post_data['pod_name'][0])
        # print(type(lines))
        return JsonResponse(lines, status=status.HTTP_200_OK)

def get_google_access_token(service_account_file):
    credentials = google.oauth2.service_account.Credentials.from_service_account_file(
        service_account_file,
        scopes=["https://www.googleapis.com/auth/logging.read"]
    )
    auth_request = google.auth.transport.requests.Request()
    credentials.refresh(auth_request)
    return credentials.token


def consecutive_loop(mydict:{}):
    all_results = []
    consecutive_empty_results = 0
    max_empty_checks = 3  # Number of empty checks before assuming logs are complete

    while consecutive_empty_results < max_empty_checks:
        try:
            # Make the request with a timeout
            response = requests.post(mydict['url'], headers=mydict['headers'], data=json.dumps(mydict['payload']), timeout=60)
            response.raise_for_status()
            data = response.json()

            # Extract logs
            results = data.get('entries', [])
            all_results.extend(results)

            # Check if new logs were found
            if results:
                print(f"Fetched {len(results)} new log entries. {datetime.datetime.now().date()}")
                # print("results", results, str(datetime.datetime.now()), "\n")
                consecutive_empty_results = 0  # Reset counter if new logs are found
            else:
                print("No new logs found. Checking again...")
                consecutive_empty_results += 1

            # Wait before querying again
            time.sleep(10)  # Wait for 10 seconds before checking again

        except requests.exceptions.RequestException as e:
            print(f"Request failed: {e}")
            break
    return all_results


def while_loop(mydict:{}):
    all_entries = []
    # Loop to handle pagination
    while True:
        # Make the API request
        response = requests.post(mydict['url'], headers=mydict['headers'], data=json.dumps(mydict['payload']))

        # Check if the request was successful
        if response.status_code == 200:
            data = response.json()
            # Append the retrieved entries to the list
            all_entries.extend(data.get("entries", []))
            next_page_token = data.get("nextPageToken")
            if next_page_token:
                # print("Next Page Token", next_page_token, datetime.datetime.utcnow())
                mydict['payload']["pageToken"] = next_page_token
            else:
                # print("No more pages to fetch, break out of the loop", datetime.datetime.utcnow())
                break
        else:
            print(f"Failed to retrieve logs. Status code: {response.status_code}, Error: {response.text}")
            break
    return all_entries


def get_pod_name_log_info(pod_name):
    current_dir=f"{os.path.dirname(os.path.abspath(__file__))}"
    service_account_file = f"{os.environ['DJANGO_ROOT_DIR']}/credentials/google_access_conf.json"
    access_token = get_google_access_token(service_account_file)

    url = "https://logging.googleapis.com/v2/entries:list"

    headers = {
        "Authorization": f"Bearer {access_token}",
        "Content-Type": "application/json"
    }
    now = datetime.datetime.utcnow()
    two_hours_ago = now - datetime.timedelta(hours=2)
    time_filter = f'timestamp >= "{two_hours_ago.isoformat("T")}Z"'

    # Define the request body (filtering by pod name)
    payload = {
        "projectIds": ["reporting-data-base"],
        "filter": (
            f'resource.labels.namespace_name="argo" '
            f'AND resource.labels.pod_name=~"{pod_name}" '
            f'AND {time_filter} '
            'AND resource.labels.container_name!="wait" '
            'AND resource.labels.container_name!="init" '
        ),
        "orderBy": "timestamp desc",
        "pageSize": 5000
    }

    all_entries = []
    all_entries += while_loop({"url":url, "headers":headers, "payload":payload})

    # Process the results (for example, print all entries)
    all_payloads = []
    ignored_text = []
    # ignored_text.append("INFO     [logger.py")
    ignored_text.append("logger.py")
    ignored_text.append("Sent all pending logs.")
    ignored_text.append("Waiting up to 5 seconds.")
    ignored_text.append(f"""argo=true error=""")

    for entry in all_entries:
        if 'textPayload' in entry.keys():
            dont = False
            for ln in ignored_text:
                if ln in entry['textPayload'] :
                    dont = True
                    continue
            if not dont: all_payloads.append(entry['textPayload'])
    return {"entries": all_payloads}
