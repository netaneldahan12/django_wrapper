from django.db import models

# Create your models here.
# class Brand_Details(models.Model):
#     class Meta:
#         app_label = 'brands'
#         db_table = 'brand_details'
#
#     # use_qftd_as_ftd = models.SmallIntegerField(null=True, default=0)
#     # token = models.CharField(max_length=70, null=True)
#     # subbrandid = models.CharField(max_length=45, null=True)
#     # report_kind = models.CharField(max_length=20, choices=Report_Types.choices, null=True)
#     # prefixed_player_id = models.CharField(max_length=100, null=True)
#     passwd_valid = models.SmallIntegerField(null=True, default=0)
#     id = models.BigIntegerField(primary_key=True)
#     # deal_kind = models.CharField(max_length=20, choices=Deal_Types.choices, null=True, default=Deal_Types.date_based)
#     # days_chunk = models.IntegerField(null=True, default=0)
#     # clk_alias = models.CharField(max_length=45, null=True)
#     brand_group_details_id = models.IntegerField(default=-1, null=True)
#     brand_display_name = models.CharField(max_length=45, null=True)
#     brand_id = models.BigIntegerField(default=-1, null=True)
#     # backend_interface_group = models.CharField(max_length=45, null=True)
#     # Modified = models.DateTimeField(null=True)
#     # Gdrive_Folder = models.CharField(max_length=256, null=True)
#
#     # def __str__(self):
#     #     return self.brand_display_name
#
# class Brand_Grouping(models.Model):
#     class Meta:
#         app_label = 'brands'
#         db_table = 'brand_group_details'
#
#     # use_qftd_as_ftd = models.SmallIntegerField(null=True, default=0)
#     # token = models.CharField(max_length=70, null=True)
#     # subbrandid = models.CharField(max_length=45, null=True)
#     # report_kind = models.CharField(max_length=20, choices=Report_Types.choices, null=True)
#     # prefixed_player_id = models.CharField(max_length=100, null=True)
#     # passwd_valid = models.SmallIntegerField(null=True, default=0)
#     id = models.BigIntegerField(primary_key=True)
#     # deal_kind = models.CharField(max_length=20, choices=Deal_Types.choices, null=True, default=Deal_Types.date_based)
#     # days_chunk = models.IntegerField(null=True, default=0)
#     # clk_alias = models.CharField(max_length=45, null=True)
#     brand_grouping_name = models.CharField(max_length=45, null=True)
#     backend_interface = models.CharField(max_length=45, null=True)
#     # brand_display_name = models.CharField(max_length=45, null=True)
#     # brand_id = models.BigIntegerField(default=-1, null=True)
#     # backend_interface_group = models.CharField(max_length=45, null=True)
#     # Modified = models.DateTimeField(null=True)
#     # Gdrive_Folder = models.CharField(max_length=256, null=True)
#
#     def __str__(self):
#         return self.brand_grouping_name
