import os
import sys
import datetime

currentdir= f"{os.path.dirname(os.path.abspath(__file__))}"
parentdir = f"{os.path.abspath(os.path.join(currentdir, os.pardir))}{os.sep}"
credentials_dir = f"{parentdir}credentials"

from google.cloud import storage
from google.cloud.storage import Blob, blob


class GoogleStorage:
        """
        Class implements wrapper for Google Secrets Service
        """
        PROJECT = 'reporting-data-base'
        BUCKET_PATH = 'conversion_logs'

        def __init__(self ) -> None:
            credentials_file = credentials_dir+"/google_access_conf.json"
            self.credentials = credentials_file
            self.client = storage.Client.from_service_account_json(credentials_file)

        def upload_to_bucket(self, destination_path: str, source_file: str, **kwargs):
            """
            Upload file to GCS
            {"report_start_date": 2024-05-05,
            "report_end_date": 2024-05-10}
            """
            # logger.debug(f"Upload '{destination_path}'")
            try:
                bucket = self.client.get_bucket(self.BUCKET_PATH)
                blob = bucket.blob(destination_path)
                blob.metadata = kwargs
                blob.upload_from_filename(source_file)
                # print("blob >>>>> ", blob.public_url)
                signed_url= self.get_signed_url(blob.name)
                # print(signed_url)
                return signed_url
                # logger.debug(f"Uploaded - {destination_path}")
            except Exception as ex:
                raise ex

        def get_signed_url(self, bucket_obj: str) -> str:
            """Returns Signed URL of GCS Obj"""
            try:
                bucket = self.client.get_bucket(self.BUCKET_PATH)
                blob = bucket.blob(bucket_obj)
                url = blob.generate_signed_url(
                    version="v4",
                    # This URL is valid for 7 days
                    expiration=datetime.timedelta(days=7),
                    # Allow GET requests using this URL.
                    method="GET"
                )
            except Exception as ex:
                raise ex
            return url



if __name__ == '__main__':
    gs = GoogleStorage()
    destination_path = f"{datetime.datetime.now().year}/{datetime.datetime.now().month}/google_conversions_uploader_113147_2024-07-31.log"
    gs.upload_to_bucket(destination_path=destination_path, source_file="/home/josh/Downloads/Conversions/google_conversions_uploader_113147_2024-07-31.log")
    sys.exit(0)